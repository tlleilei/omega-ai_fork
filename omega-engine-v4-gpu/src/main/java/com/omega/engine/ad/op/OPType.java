package com.omega.engine.ad.op;

public enum OPType {
	add,
	subtraction,
	scalarSubtraction,
	multiplication,
	division,
	scalarDivision,
	log,
	sin,
	cos,
	tan,
	atan,
	exp,
	dot,
	get,
	pow,
	sum,
	clamp,
	maximum,
	minimum
}
